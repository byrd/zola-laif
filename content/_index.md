+++

title = "Laif is awesome!"

description = "Laif splash page"

# Used to sort pages by "date", "weight" or "none". See below for more information.
sort_by = "none"

# Used by the parent section to order its subsections.
# Lower values have higher priority.
weight = 0

# Template to use to render this section page.
template = "section.html"

# The given template is applied to ALL pages below the section, recursively.
# If you have several nested sections, each with a page_template set, the page
# will always use the closest to itself.
# However, a page's own `template` variable will always have priority.
# Not set by default.
# page_template =

# This sets the number of pages to be displayed per paginated page.
# No pagination will happen if this isn't set or if the value is 0.
paginate_by = 0

# If set, this will be the path used by the paginated page. The page number will be appended after this path.
# The default is page/1.
paginate_path = "page"

# This determines whether to insert a link for each header like the ones you can see on this site if you hover over
# a header.
# The default template can be overridden by creating an `anchor-link.html` file in the `templates` directory.
# This value can be "left", "right" or "none".
insert_anchor_links = "none"

# If set to "true", the section pages will be in the search index. This is only used if
# `build_search_index` is set to "true" in the Zola configuration file.
in_search_index = true

# If set to "true", the section homepage is rendered.
# Useful when the section is used to organize pages (not used directly).
render = true

# This determines whether to redirect when a user lands on the section. Defaults to not being set.
# Useful for the same reason as `render` but when you don't want a 404 when
# landing on the root section page.
# Example: redirect_to = "documentation/content/overview"
# redirect_to = 

# If set to "true", the section will pass its pages on to the parent section. Defaults to `false`.
# Useful when the section shouldn't split up the parent section, like
# sections for each year under a posts section.
transparent = false

# Use aliases if you are moving content but want to redirect previous URLs to the
# current one. This takes an array of paths, not URLs.
aliases = []

# If set to "true", a feed file will be generated for this section at the
# section's root path. This is independent of the site-wide variable of the same
# name. The section feed will only include posts from that respective feed, and
# not from any other sections, including sub-sections under that section.
generate_feed = false

+++

# Your own data.
[extra]

#Laif is wonderful!
Some content here

+++
title = "BioPilia Living Design"
date = 2020-07-26
+++

# *Thy Laif Prodjekt*
***The Ecology Of WoMan As Art.***

## *She Is Her Ancestors Wildest Dreams; A Unique OpportUnity For The Creation Of Something New And Beautiful.*
***ReValuation of life, relationships, experiences, and soul-care.***

### *It's time to step out from the story of humanities present situation, and write a New Future. A new story about creating; naturaly healing, living spaces and connecting Soil to Soul. Turning the tables from an environMental withdraw to an environMental deposit for the future.*

#### **Mission: Personal empowerment through ecological literacy, to awaken possibility.**
**Knowledge to design a more desirable, fulfilling; Ecologically Modern and Realistic Lifestyle.**

### *BioPhilia Living Design* (since 1994)
***The relationship between human and environment is highlighted, not human and 'things'.***
*Private Remote Consulting / Live & Online Gatherings: 'Salon Style' Experiences*

#### *Vision: Supporting WoMan Creating A Legacy With Mother Earth.*
**One's Dream And The Dream Of Mother Earth As One.**

#### *The way out is personal responsibility:*
**A Self-creative Exploration to insight deep and meaningful change by working with the individuals natural enthusiasm.**

#### *Cultivate A Legacy:*
**Curated to attract those individuals who are genuine in their effort to reach their own potential and help others do so in the process.**

#### Custom Salon Experiences Available-
**CoCurate: Private, intimate virtual Salon Gatherings for Ritual, Ceremony, Festival, Exhibition ...**

#### CommUnity Support.
**Come together in Common Unity "Online Global CommUnity for Local Impact".**

**Private 'Vivarium Womans Salon'; invite only, please inquire.**

#### Explore ~ Connect on Matrix.org Via "Element" App.
Matrix: An open network for secure, decentralized communication.
In order to communicate through matrix, one needs to choose a client, register an account, and join a room. Ana's personal preference is "Element" app.

[**Live Blog**](https://matrix.to/#/!EwezVvVjpxKVCMIuRM:matrix.org?via=matrix.org&via=kde.org&via=converser.eu)

[**Live Interactive Community**](https://matrix.to/#/!LSpVaMCiYQehpJONFF:matrix.org?via=matrix.org)

[**Connect Directly with Ana**](https://matrix.to/#/!ibYXXCkubbZiWtkmhX:matrix.org?via=matrix.org)
...

#### [**Public: Airbnb Profile & Reviews**](https://www.airbnb.com/users/show/3774308)

#### [*Energy exchange with Paypal*](https://www.paypal.me/vivarium)

